﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Profile;
using System.Windows.Forms;

namespace Calendar1
{
    public partial class UserControlDays : UserControl
    {
        private Dictionary<string, string> _eventDictionary = new Dictionary<string, string>();
        private Dictionary<int, string> dataDictionary = new Dictionary<int, string>();

        public UserControlDays()
        {
            InitializeComponent();
        }





        private void UserControlDays_Load(object sender, EventArgs e)
        {

        }

        private void lbdays_Click(object sender, EventArgs e)
        {

        }
        public void days(int day)
        {
            lbdays.Text = day.ToString();
        }
        public void SetDayName(string dayName)
        {
            label1.Text = dayName;
        }
        public string GetDayName()
        {
            return label1.Text;

        }
        public string GetDay()
        {
            return lbdays.Text;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lbdays_Click_1(object sender, EventArgs e)
        {
            string day = GetDay(); // Отримати номер дня

            //MessageBox.Show("Номер дня: " + day);
            EventForm eventForm = new EventForm(day);
            eventForm.ShowDialog();
        }

        private void btnShowData_Click(object sender, EventArgs e)
        {
            string day = GetDay();
            ShowDataForDay(day);



        }
        private void ShowDataForDay(string day)
        {
            // Відкрити файл data.txt
            string filePath = "C:\\Users\\Asus\\OneDrive\\Документи\\data.txt";

            if (File.Exists(filePath))
            {
                // Зчитати весь вміст файлу
                string fileContent = File.ReadAllText(filePath);

                // Розділити вміст на рядки
                string[] lines = fileContent.Split('\n');

                // Перебрати рядки та знайти значення 
                List<string> values = new List<string>();

                foreach (string line in lines)
                {
                    // Розділити рядок на ключ та значення (припускається, що вони розділені символом ':')
                    string[] parts = line.Split(':');
                    if (parts.Length == 2)
                    {
                        string key = parts[0].Trim();
                        string value = parts[1].Trim();

                        if (key == day)
                        {
                            values.Add(value);
                        }
                    }
                }

                // Вивести значення ключа у MessageBox
                if (values.Count > 0)
                {
                    string valuesString = string.Join("\n", values);
                    MessageBox.Show("Події на цю дату: " + valuesString);
                }
                else
                {
                    MessageBox.Show("Немає даних для вибраної дати.");
                }
            }
            else
            {
                MessageBox.Show("Файл data.txt не знайдено.");
            }
        }
    }
}