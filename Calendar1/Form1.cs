﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Calendar1
{
    public partial class Form1 : Form
    {
        int month, year;


        public Form1()
        {
            InitializeComponent();
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            displaDays();
        }
        private void displaDays()
        {
            DateTime now = DateTime.Now;
            month = now.Month;
            year = now.Year;
            String monthname = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
            LBDATE.Text = monthname + "" + year;
            DateTime startofthemonth = new DateTime(year, month, 1);
            int days = DateTime.DaysInMonth(year, month);
            int dayoftheweek = (int)startofthemonth.DayOfWeek;

            if (dayoftheweek == 0)
                dayoftheweek = 7;

            for (int i = 1; i < dayoftheweek; i++)
            {
                UserControlBlank ucblank = new UserControlBlank();
                daycontainer.Controls.Add(ucblank);
            }

            for (int i = 1; i <= days; i++)
            {
                UserControlDays ucdays = new UserControlDays();
                ucdays.days(i);
                DateTime currentDate = new DateTime(year, month, i);
                string dayName = currentDate.ToString("dddd");
                ucdays.SetDayName(dayName);
                daycontainer.Controls.Add(ucdays);
            }

        }

        private void btnprevious_Click(object sender, EventArgs e)
        {
            daycontainer.Controls.Clear();
            month--;
            if (month == 0)
            {
                month = 12;
                year--;
            }

            String monthname = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
            LBDATE.Text = monthname + "" + year;
            DateTime startofthemonth = new DateTime(year, month, 1);
            int days = DateTime.DaysInMonth(year, month);
            int dayoftheweek = (int)startofthemonth.DayOfWeek;

            if (dayoftheweek == 0)
                dayoftheweek = 7;

            for (int i = 1; i < dayoftheweek; i++)
            {
                UserControlBlank ucblank = new UserControlBlank();
                daycontainer.Controls.Add(ucblank);
            }

            for (int i = 1; i <= days; i++)
            {
                UserControlDays ucdays = new UserControlDays();
                ucdays.days(i);
                DateTime currentDate = new DateTime(year, month, i);
                string dayName = currentDate.ToString("dddd");
                ucdays.SetDayName(dayName);
                daycontainer.Controls.Add(ucdays);
            }

        }

        private void вИХІДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Текстові файли (.txt)|.txt";
            saveFileDialog.Title = "Зберегти файл";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StringBuilder data = new StringBuilder();

                foreach (Control control in daycontainer.Controls)
                {
                    if (control is UserControlDays ucdays)
                    {
                        string day = ucdays.GetDay();
                        string dayName = ucdays.GetDayName();
                        string monthName = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
                        string yearString = year.ToString();

                        string line = $"{dayName}, {day} {monthName} {yearString}";
                        data.AppendLine(line);
                    }
                }

                string filePath = saveFileDialog.FileName;
                File.WriteAllText(filePath, data.ToString());
                MessageBox.Show("Файл збережено успішно.");
            }
        }

        

        private void завантажитиВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void daycontainer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            daycontainer.Controls.Clear();
            month++;
            if (month == 13)
            {
                month = 1;
                year++;
            }

            String monthname = DateTimeFormatInfo.CurrentInfo.GetMonthName(month);
            LBDATE.Text = monthname + "" + year;
            DateTime startofthemonth = new DateTime(year, month, 1);
            int days = DateTime.DaysInMonth(year, month);
            int dayoftheweek = (int)startofthemonth.DayOfWeek;

            if (dayoftheweek == 0)
                dayoftheweek = 7;

            for (int i = 1; i < dayoftheweek; i++)
            {
                UserControlBlank ucblank = new UserControlBlank();
                daycontainer.Controls.Add(ucblank);
            }

            for (int i = 1; i <= days; i++)
            {
                UserControlDays ucdays = new UserControlDays();
                ucdays.days(i);
                DateTime currentDate = new DateTime(year, month, i);
                string dayName = currentDate.ToString("dddd");
                ucdays.SetDayName(dayName);
                daycontainer.Controls.Add(ucdays);
            }

        }



       
    }

}