﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.UI;
using Newtonsoft.Json;

namespace Calendar1
{
    public partial class EventForm : Form
    {
        private string selectedDay;
        private Dictionary<string, string> dataDictionary = new Dictionary<string, string>();
        public EventForm(string day)
        {
            InitializeComponent();
            selectedDay = day;

        }

        private void EventForm_Load(object sender, EventArgs e)
        {
            textBoxDay.Text = selectedDay;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string key = textBoxDay.Text; // Отримати значення ключа з textBoxDay
            string value = textBox1.Text; // Отримати значення для ключа з textBox1

            if (!string.IsNullOrEmpty(key))
            {
                // Додати або оновити запис у словнику
                if (dataDictionary.ContainsKey(key))
                {
                    dataDictionary[key] = value;
                }
                else
                {
                    dataDictionary.Add(key, value);
                }

            }

            // Очистити тексти вводу
            textBoxDay.Text = "";
            textBox1.Text = "";
            SaveDictionaryToFile(dataDictionary);
        }

        private void SaveDictionaryToFile(Dictionary<string, string> dictionary)
        {
            string filePath = "C:\\Users\\Asus\\OneDrive\\Документи\\data.txt";
           
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                foreach (var item in dictionary)
                {
                    writer.WriteLine($"{item.Key}:{item.Value}");
                }
            }

            MessageBox.Show("Збережено");

        }

    }
}
